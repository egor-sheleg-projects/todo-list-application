﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace TODODomainModel
{
    public class ApplicationContext : DbContext
    {
        public DbSet<TODOList> TODOLists { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=firstdatabase;Trusted_Connection=True;");
        }
    }
}
