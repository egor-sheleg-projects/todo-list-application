﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TODODomainModel
{
    public class TODOList
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<TODOTask> Tasks { get; set; } = new List<TODOTask> { };
    }
}
