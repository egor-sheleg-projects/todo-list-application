﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TODODomainModel
{
    public class TODOTask
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime Date { get; set; }

        public bool IsComplete { get; set; }

        public string Tasks { get; set; }
    }
}
