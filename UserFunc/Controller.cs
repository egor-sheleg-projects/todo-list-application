﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TODODomainModel;

namespace UserFunc
{
    public static class Controller
    {
        public static bool CreateList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var list = new TODOList { Name = nameList };

                db.TODOLists.AddRange(list);
                db.SaveChanges();
            }

            return true;
        }

        public static bool AddTaskToList(string nameList, string nameTask, DateTime dateTime, string entertask)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();

                if (lists != null) 
                {
                    List<TODOTask> tasks = new List<TODOTask>();

                    TODOTask task = new TODOTask();
                    task.Name = nameTask;
                    task.Tasks = entertask;
                    task.Date = dateTime;
                    task.IsComplete = false;
                    tasks.Add(task);

                    lists.Tasks.AddRange(tasks);
                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
                
            }

            return true;
        }

        public static bool DeleteTaskFromList(string nameList, string taskname)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();

                if (lists != null)
                {
                    List<TODOTask> tasks = lists.Tasks;

                    TODOTask task = tasks.Find(x => x.Name == taskname);
                    tasks.Remove(task);

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool DeleteList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();
                if (lists != null)
                {
                    db.Remove(lists);

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool UpdateTaskInList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();

                if (lists != null)
                {
                    string button = string.Empty;

                    List<TODOTask> tasks = lists.Tasks;

                    Console.WriteLine("Enter task name to edit:");
                    string taskname = Console.ReadLine();
                    TODOTask task = tasks.Find(x => x.Name == taskname);
                    Console.WriteLine("Enter task new name:");
                    button = Console.ReadLine();
                    task.Name = button;
                    Console.WriteLine("Enter new task:");
                    button = Console.ReadLine();
                    task.Tasks = button;
                    int i = 0;

                    do
                    {
                        Console.WriteLine("Enter date to end of the task:");
                        DateTime userDateTime;
                        if (DateTime.TryParse(Console.ReadLine(), out userDateTime))
                        {
                            task.Date = userDateTime;
                            task.IsComplete = false;
                            i = 1;
                        }
                        else
                        {
                            Console.WriteLine("You have entered an incorrect value.");
                        }
                    }
                    while (i == 0);

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool UpdateList(string nameList, string newnameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();

                if (lists != null)
                {

                    lists.Name = newnameList;

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool CompleteTask(string nameList, string taskname)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();

                if (lists != null)
                {
                    List<TODOTask> tasks = lists.Tasks;

                    TODOTask task = tasks.Find(x => x.Name == taskname);
                    task.IsComplete = true;

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static bool CompleteList(string nameList)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).Where(z => z.Name == nameList).FirstOrDefault<TODOList>();

                if (lists != null)
                {
                    List<TODOTask> tasks = lists.Tasks;

                    foreach (TODOTask t in tasks)
                    {
                        t.IsComplete = true;
                    }

                    db.SaveChanges();
                }
                else
                {
                    throw new ArgumentNullException(nameof(nameList));
                }
            }

            return true;
        }

        public static void GetLists()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var lists = db.TODOLists.Include(x => x.Tasks).ToList();
                foreach (TODOList i in lists)
                {
                    Console.WriteLine($"---{i.Name}---");

                    if (i.Tasks != null)
                    {
                        foreach (var s in i.Tasks)
                        {
                            Console.WriteLine($"{s.Name} --- {s.Tasks}| Date:{s.Date}| Is completed:{s.IsComplete}");
                        }
                    }
                }
            }
        }
    }
}
